Allows the Form API to create taxonomy autocomplete textfields for vocabularies
with no term reference fields set up.

Once enabled this module allows the creation of Form API autocomplete textfields
using the following code:

$vocab = taxonomy_vocabulary_machine_name_load('vocabulary_machine_name');
  
$form['textfield_name'] = array(
  '#title' => t('Textfield Title'),
  '#type' => 'textfield',
  '#autocomplete_path' => 'taxonomy_vid/autocomplete/' . $vocab->vid,
  '#element_validate' => array('taxonomy_autocomplete_validate'), 
)
